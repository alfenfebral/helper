<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Filtering bad words
function filter_bad_words($words) {
	$swears = array(
		"fuck" => "f**k",
		"damn" => "d**n",
		"shit" => "s**t"
	); // swears

	$result = str_ireplace(array_keys($swears), array_values($swears), $words);
	return $result;
}